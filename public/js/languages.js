language_strings = {

	'en_US': {
		'out_of_invoice_tries': 'An error occurred (code -1.1). Please try later.',
		'last_events_widget_days_ago': {
			0: '{0} day ago',
			1: '{0} days ago',
		},
		'last_events_widget_hours_ago': {
			0: '{0} hour ago',
			1: '{0} hours ago',
		},
		'last_events_widget_minutes_ago': {
			0: '{0} minute ago',
			1: '{0} minutes ago',
		},
		'last_events_widget_seconds_ago': {
			0: '{0} second ago',
			1: '{0} seconds ago',
		},
		'donation_goal_widget_days_left': {
			0: '{0} day left',
			1: '{0} days left',
		},
		'donation_goal_widget_hours_left': {
			0: '{0} hour left',
			1: '{0} hours left',
		},
		'donation_goal_widget_minutes_left': {
			0: '{0} minute left',
			1: '{0} minutes left',
		},
		'donation_goal_widget_seconds_left': {
			0: '{0} second left',
			1: '{0} seconds left',
		},
		'last_events_widget_via': 'via {0}',
		'last_events_widget_for': 'for {0}',
		'last_events_widget_months_in_a_row': {
			0: '{0} month in a row',
			1: '{0} months in a row',
		},
		'file_manager_error_exceeded_max_size': 'The size of the uploading file should not exceed {0} Mb!',
		'file_manager_error_forbidden_extension': 'Only files with JPG, JPEG, GIF, PNG, MP3, WAV and OGG extensions are allowed!',
		'file_manager_delete_file_warning': 'Are you sure you want to delete this file? Please make sure that you don\'t use this file anywhere.',
		'anonymous': 'Anonymous',
		'giveaway_cancel_warning': 'Are you sure you want to cancel this giveaway?',
		'alert_variations_delete_warning': 'Are you sure you want to delete this variation?',
		'alert_variations_clear_queue': 'Are you sure you want to clear the alert queue of this type?',
		'payouts_delete_warning': 'Are you sure you want to delete this payout method?',
		'iss_delete_widget_warning': 'Are you sure you want to delete this widget?',
		'donation_goals_stop_warning': 'Are you sure you want to stop this goal?',
		'media_preview_play_seconds': {
			0: '{0} second',
			1: '{0} seconds',
		},
		'polls_stop_warning': 'Are you sure you want to stop this poll?',
		'polls_remove_option': 'Remove option',
		'polls_variant': 'Option {0}:',
		'polls_variant_title': 'Option title',
		'donations_delete_warning': 'Are you sure you want to delete this donation?',
		'alerts_test_message': 'The test alert will display in a few seconds.',
		'two_factor_hold_seconds': {
			0: '{0} second',
			1: '{0} seconds',
		},
		'connections_disconnect_warning': 'Are you sure you want to disconnect this account?',
		'reset_token_warning': 'Are you sure you want to reset token? Please note that the links to all widgets will change!',
		'widget_link_removed': '(link removed)',
		'donation_goal_widget_goal_over': 'Goal is over!',
		'alerts_widget_months': {
			0: '{0} month',
			1: '{0} months',
		},
		'cookies_warning_text': 'By continuing your visit on our website, we assume your permission to deploy cookies to guarantee a better experience on our site.',
		'cookies_warning_learn_more': 'Learn more',
		'cookies_warning_hide': 'Hide',
	},

	'ru_RU': {
		'out_of_invoice_tries': 'Произошла ошибка (код -1.1). Пожалуйста, попробуйте позже.',
		'last_events_widget_days_ago': {
			0: '{0} день назад',
			1: '{0} дня назад',
			2: '{0} дней назад',
		},
		'last_events_widget_hours_ago': {
			0: '{0} час назад',
			1: '{0} часа назад',
			2: '{0} часов назад',
		},
		'last_events_widget_minutes_ago': {
			0: '{0} минуту назад',
			1: '{0} минуты назад',
			2: '{0} минут назад',
		},
		'last_events_widget_seconds_ago': {
			0: '{0} секунду назад',
			1: '{0} секунды назад',
			2: '{0} секунд назад',
		},
		'donation_goal_widget_days_left': {
			0: 'Остался {0} день',
			1: 'Осталось {0} дня',
			2: 'Осталось {0} дней',
		},
		'donation_goal_widget_hours_left': {
			0: 'Остался {0} час',
			1: 'Осталось {0} часа',
			2: 'Осталось {0} часов',
		},
		'donation_goal_widget_minutes_left': {
			0: 'Осталась {0} минута',
			1: 'Осталось {0} минуты',
			2: 'Осталось {0} минут',
		},
		'donation_goal_widget_seconds_left': {
			0: 'Осталась {0} секунда',
			1: 'Осталось {0} секунды',
			2: 'Осталось {0} секунд',
		},
		'last_events_widget_via': 'через {0}',
		'last_events_widget_for': 'за {0}',
		'last_events_widget_months_in_a_row': {
			0: '{0} месяц подряд',
			1: '{0} месяца подряд',
			2: '{0} месяцев подряд'
		},
		'file_manager_error_exceeded_max_size': 'Размер загружаемого файла не должен превышать {0} Мб!',
		'file_manager_error_forbidden_extension': 'Принимаются файлы только с расширениями JPG, JPEG, GIF, PNG, MP3, WAV и OGG!',
		'file_manager_delete_file_warning': 'Вы действительно хотите удалить этот файл? Убедитесь, что этот файл Вами нигде не используется.',
		'anonymous': 'Аноним',
		'giveaway_cancel_warning': 'Вы действительно отменить этот розыгрыш?',
		'alert_variations_delete_warning': 'Вы действительно хотите удалить эту вариацию?',
		'alert_variations_clear_queue_warning': 'Вы действительно хотите очистить очередь оповещений этого типа?',
		'payouts_delete_warning': 'Вы действительно хотите удалить этот способ выплат?',
		'iss_delete_widget_warning': 'Вы действительно хотите удалить этот виджет?',
		'donation_goals_stop_warning': 'Вы действительно хотите остановить этот сбор?',
		'media_preview_play_seconds': {
			0: '{0} секунда',
			1: '{0} секунды',
			2: '{0} секунд',
		},
		'polls_stop_warning': 'Вы действительно хотите остановить голосование?',
		'polls_remove_option': 'Удалить вариант',
		'polls_variant': 'Вариант {0}:',
		'polls_variant_title': 'Название варианта',
		'donations_delete_warning': 'Вы действительно хотите удалить это сообщение?',
		'alerts_test_message': 'Тестовое оповещение отобразится в течение следующих нескольких секунд.',
		'two_factor_hold_seconds': {
			0: '{0} секунду',
			1: '{0} секунды',
			2: '{0} секунд',
		},
		'connections_disconnect_warning': 'Вы действительно хотите отсоединить аккаунт?',
		'reset_token_warning': 'Вы действительно обновить токен? Обратите внимание, что ссылки на все виджеты изменятся!',
		'widget_link_removed': '(ссылка удалена)',
		'donation_goal_widget_goal_over': 'Сбор завершён!',
		'alerts_widget_months': {
			0: '{0} месяц',
			1: '{0} месяца',
			2: '{0} месяцев'
		},
		'cookies_warning_text': 'Продолжая использование нашего сайта, мы подразумеваем Ваше разрешение на использование файлов cookie, чтобы предоставить Вам наилучший опыт использования сайта.',
		'cookies_warning_learn_more': 'Узнайте подробности',
		'cookies_warning_hide': 'Скрыть',
	}

}

language_plural = {
	'en_US': '(n != 1)',
	'ru_RU': '(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)',
}

function translateString(string, language) {
	var language = (typeof language === 'undefined') ? document_language : language;

	if (typeof string !== 'undefined' && string !== null && typeof language !== 'undefined' && language !== null && language in language_strings && string in language_strings[language]) {
		return language_strings[language][string];
	}else{
		return string;
	}
}

function translateStringPlural(string, n, language) {
	var language = (typeof language === 'undefined') ? document_language : language;
	var n = parseInt(n);

	var plural_form = eval(language_plural[language]);

	if (plural_form === true) {
		plural_form = 1;
	} else if (plural_form === false) {
		plural_form = 0;
	}

	if (typeof string !== 'undefined' && string !== null && typeof language !== 'undefined' && language !== null && language in language_strings && string in language_strings[language] && plural_form in language_strings[language][string]) {
		return language_strings[language][string][plural_form];
	}else{
		return string;
	}
}